'use strict';

describe('Service: congreso', function () {

  // load the service's module
  beforeEach(module('gerenglobAnalyticsApp'));

  // instantiate service
  var congreso;
  beforeEach(inject(function (_congreso_) {
    congreso = _congreso_;
  }));

  it('should do something', function () {
    expect(!!congreso).toBe(true);
  });

});
