'use strict';

describe('Controller: DataanalysisCtrl', function () {

  // load the controller's module
  beforeEach(module('gerenglobAnalyticsApp'));

  var DataanalysisCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DataanalysisCtrl = $controller('DataanalysisCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
