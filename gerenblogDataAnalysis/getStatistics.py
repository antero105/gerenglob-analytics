import requests
from pymongo import MongoClient
import time
import logging
import io, json


def getStatistics():
	events = requests.get('http://app.gerenglob.com/get_statistics/all/892c888e1054043b02a952886c28fcf7/eventos.json').json()
	records = requests.get('http://app.gerenglob.com/get_statistics/all/892c888e1054043b02a952886c28fcf7/registros.json').json()
	return events,records

def updateDB(congresos,registros):
	client = MongoClient()
	db = client.gerenglob	
	for congreso in congresos:
		db.congresos.update({'congreso':congreso['congreso'],'edicion':congreso['edicion']},congreso,True)
		updateLog('Updated Congreso :'+congreso['congreso']+', at: '+time.strftime("%d/%m/%y"))
	for registro in registros:
		db.registros.update({'ciudad':registro['ciudad'],'universidad':registro['universidad']},registro,True)
		updateLog('Updated Registro:'+registro['congreso']+','+registro['universidad']+', At: '+time.strftime("%d/%m/%y"))
	print 'End...'

def updateLog(text):
	LOG_FILENAME = 'test.log'
	logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
	logging.debug(text)

def main():
	congresos,registros = getStatistics()
	#updateDB(congresos,registros)
	newUpdateDB(congresos,registros)

def newUpdateDB(congresos,registrosdb):
	client = MongoClient()
	db = client.gerenglob	
	response = []
	response_json = {
		'nombre':'',
		'ediciones':[]
	}
	tem_array = []
	client = MongoClient()
	db  = client.gerenglob
	count = 0
	
	for index, congreso in enumerate(congresos):
		#response_json.congreso = congreso['congreso']
		
		if congreso['congreso'] not in tem_array:
			tem_array.append(congreso['congreso'])

	for index,item in enumerate(tem_array):
		response_json['nombre'] = item
		
		for congreso in congresos:
			
			if item == congreso['congreso']:
				response_json['ediciones'].append({
						'id_evento':congreso['id_evento'],
						'edicion' : congreso['edicion'],
						'n_reservas':congreso['n_reservas'],
						'n_inscritos':congreso['n_inscritos'],
						'n_pagos_completos':congreso['n_pagos_completos'],
						'n_con_habitacion':congreso['n_con_habitacion'],
						'n_acompanantes':congreso['n_acompanantes'],
						'n_pagos_disponibles':congreso['n_pagos_disponibles'],
						'n_registrados':congreso['n_registrados'],
						'fecha_evento':congreso['fecha_evento'],
						'total':congreso['total']

					})
				if response_json in response:
					pass
				else:
					response.append(response_json)
		response_json = {
			'nombre':'',
			'ediciones':[]
		}
	

	
	for r in response:
		db.congresos.update({'nombre':r['nombre']},r,True)
		updateLog('Updated Congreso :'+congreso['congreso']+', at: '+time.strftime("%d/%m/%y"))
	for registro in registrosdb: 
		db.registros.update({'id_evento':registro['id_evento'],'congreso':registro['congreso'],'universidad':registro['universidad'],'ciudad':registro['ciudad'],'estado':registro['estado']},registro,True,True)
		updateLog('Updated Registro:'+registro['congreso']+','+registro['universidad']+', At: '+time.strftime("%d/%m/%y"))
	
	
	print 'End...'
		







		
	
main()
	






