from bottle import Bottle, request, response, run,route
from pymongo import MongoClient
import json
from bson.json_util import dumps

app = Bottle()

@app.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

 


@app.route('/getCongresoStatistics/<congreso>',method=['OPTIONS','GET'	])
def getCongresoStatistics(congreso):
    enable_cors()
	
    if request.method == 'OPTIONS':
		return {}
	
    else:	
		client = MongoClient()
		db = client.gerenglob
		cursor = db.congresos.find({'nombre':congreso})
		return dumps(cursor)

@app.route('/getCongresoRegister/<congreso>/<congresoId>',method=['OPTIONS','GET'])
def getCongresoRegister(congreso,congresoId):
    enable_cors()
    response = []
    if request.method == 'OPTIONS':
        return {}
    else:
        client = MongoClient()
        db  = client.gerenglob
        cursor = db.registros.find({'id_evento':int(congresoId), 'congreso':congreso}).sort('cant',-1).limit(20)
    
        return dumps(cursor)
        
@app.route('/getCongresosNames',method = ['OPTIONS','GET'])
def getCongresosNames():
    enable_cors()
    if request.method == 'OPTIONS':
        return {}
    else:
        client = MongoClient()
        db = client.gerenglob
        cursor = db.congresos.find({},{'nombre':1})
        return dumps(cursor)

if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("--host", dest="host", default="192.168.0.106",
                      help="hostname or ip address", metavar="host")
    parser.add_option("--port", dest="port", default=9001,
                      help="port number", metavar="port")
    (options, args) = parser.parse_args()
    run(app, host=options.host, port=int(options.port))