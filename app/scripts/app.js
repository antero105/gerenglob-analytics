'use strict';

/**
 * @ngdoc overview
 * @name gerenglobAnalyticsApp
 * @description
 * # gerenglobAnalyticsApp
 *
 * Main module of the application.
 */
angular
  .module('gerenglobAnalyticsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.select2'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/dataAnalysis/:congresoName', {
        templateUrl: 'views/dataanalysis.html',
        controller: 'DataanalysisCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
