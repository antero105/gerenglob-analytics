'use strict';

/**
 * @ngdoc function
 * @name gerenglobAnalyticsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gerenglobAnalyticsApp
 */
angular.module('gerenglobAnalyticsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
