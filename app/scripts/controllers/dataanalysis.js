'use strict';

/**
 * @ngdoc function
 * @name gerenglobAnalyticsApp.controller:DataanalysisCtrl
 * @description
 * # DataanalysisCtrl
 * Controlador graficos, se solicitan los servicios y se grafican en la vista.
 * Se declara el objeto lineData para los graficos de barra, y todo se inserta luego del request
 * 
 */




var ready = false;

var barData = {
    bindto: '#barChart',
    data: {
      columns: [],
      type: 'bar'
    },
    bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    },
    axis: {
        x: {
            type: 'category',
            categories: ['Cantidad de Inscritos','Cantidad con Reservas','Cantidad con acompañantes','Total']
        }
    },
};

var donutData = {
    bindto: '#chart_two',
    data: {
        columns: [],
        type : 'donut',
    },

    donut: {
        title: "Estados"
    }
};

var pieData = {
    bindto: '#chart_three',
    data: {
        columns: [],
        type : 'pie',
    }
};
var response  =[];

angular.module('gerenglobAnalyticsApp')
  .controller('DataanalysisCtrl', function ($scope,$routeParams,$http,$q) {
    $scope.congresoName = $routeParams.congresoName;
    if(!ready)
    $('#myModal').modal();
    
    $scope.eventActive = false;
    

    /*
        Solicitudes, y actualizar la barra de progreso
     */
    $http.get('http://192.168.0.106:9001/getCongresoStatistics/'+$routeParams.congresoName).success(function(data){
        var urls =[];
        var responses =[];
        
        angular.forEach(data[0].ediciones,function(entry){
            urls.push('http://192.168.0.106:9001/getCongresoRegister/'+$routeParams.congresoName+'/'+entry.id_evento)
        });
        
        /*
            Barra de progreso, se crea una promesa que se cumplira cuando se terminen de hacer todas las solicitudes
            se tiene un arreglo de solicitudes llamado urls. por cada una de las solicitudes se guardan en un arreglo
            llamado response. Se crea un contador para saber cuantas solicitudes fueron realizadas. 
            Para saber el % de la barra de progreso es x/(el ancho de url) + 1 (por que x empieza en 0). *100 para tenerlo en %.
         */
        var promise = $q.all(null);
        var x =0;
        angular.forEach(urls, function(url){
            promise = promise.then(function(){
                return $http({
                method: 'GET', 
                url:url 
            }).then(function(res){
                responses.push(res.data);
                x = x + 1;
                $scope.progressBar = function(){
                    return ((x/urls.length + 1) * 100).toString() + '%';
                }
            });
            });
        });
        promise.then(function(){
        
       
            $('#myModal').modal('hide');
            $('#myModal').on('hidden.bs.modal', function (e) {
                console.log('End Loading...');    
            });//Closing Modal
            ready = true;
       
            /*
                Llenando tabla
             */
            $scope.congresos = data[0].ediciones;
            /*
                ---------------------------
             */
            /*
                Click de edicion, donde se activan los graficos de cada edicion.
             */
            $scope.chooseEvent = function(id_event,edicion){
                response = responses[id_event];
                $scope.edicion = edicion;
                /*
                    Segundo Grafico: Grafico de Dona, Estados participantes en edicion.
                    Usando la libreria c3js http://www.c3js.org
                */
    
                donutData.data.columns = response.map(function(obj){
                    return [obj.estado,obj.cant]
                });
        
                var chart2 = c3.generate(donutData);
                /*
                    ---------------------------------------------------------
                 */
                
                /*
                    Tercer Grafico: Grafico de Pie, Universidades participantes en edicion.
                    Usando la libreria c3js http://www.c3js.org
                */
            
                pieData.data.columns = response.map(function(obj){
                    return [obj.universidad,obj.cant];
                });
                var chart3 = c3.generate(pieData);
                
                /*
                    ---------------------------------------------------------
                 */
                $scope.eventActive = true;
                
               
            };
            
            

            /*
                Primer Grafico: Grafico de Barra comparacion de ediciones.
                Usando la libreria c3js http://www.c3js.org
            */
             
             barData.data.columns = data[0].ediciones.map(function(obj){
                return [obj.edicion,obj.n_inscritos,obj.n_reservas,obj.n_acompanantes,obj.total]
             });

             
             var chart = c3.generate(barData);
             
            /*
                --------------------------------------------------------
             */
            
            
           
            

            /*
                Cuarto Grafico: Grafico Geografico, Lideres con mayor penetracion
             */
        });

    });
    /*
       Tabla colores.
     */
     
      $scope.congresoStats = function(n_inscritos){
        if(n_inscritos >= 200){
            return true;
        }
        else{
            return false;
        }
     }






});
