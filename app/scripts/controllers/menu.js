'use strict';

/**
 * @ngdoc function
 * @name gerenglobAnalyticsApp.controller:MenuCtrl
 * @description
 * # MenuCtrl
 * Controlador del menu, se conecta con la base de datos, solo si la informacion no se encuentra dentro de
 * local storage, lo cual puede reducir considerablemente la cantidad de solicitudes hacia el servidor.
 */
angular.module('gerenglobAnalyticsApp')
  .controller('MenuCtrl', function ($scope,$http,$location) {
        console.log('Creating the menu');
        $("#select").select2();  
    
        $scope.isActive = function(route,id) {
            return '/dataAnalysis/'+route+'/'+id === $location.path();
        }
        if(localStorage.getItem('congresos') == null){
            console.log('Getting from the server');
            $http.get('http://192.168.0.106:9001/getCongresosNames').success(function(data){
                console.log(data);
                localStorage.setItem('congresos',JSON.stringify(data));
                $scope.congresos = data;
            });
        }
        else{
            console.log('Getting from local');
            $scope.congresos =  JSON.parse(localStorage.getItem('congresos'));
        }

        $scope.selectCongreso  = function(congreso){
            $location.path('/dataAnalysis/'+congreso);
        }

    	
    	
    	
    	

  });
