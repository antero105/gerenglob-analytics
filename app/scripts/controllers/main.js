'use strict';

/**
 * @ngdoc function
 * @name gerenglobAnalyticsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gerenglobAnalyticsApp
 */
angular.module('gerenglobAnalyticsApp')
  .controller('MainCtrl', function ($scope,$location) {
    	
    	$scope.isActive = function(route) {
        	return route === $location.path();
    	}
  });
