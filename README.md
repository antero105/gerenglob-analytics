Aplicación de análisis de datos de Gerenglob C.A


Configuración:

1- Instalar MongoDB. http://www.mongodb.org/downloads

2- Crear dos colecciones en MongoDB llamadas congresos y registros. Respectivamente.

3- Dentro del repositorio hay una carpeta llamada gerenglobDataAnalysis, correr el programa getStatistics.py, este programa se encarga de recolectar toda la data de la base de datos y guardarla en las colecciones anteriormente dichas.

4- Luego en la carpeta serverSide, correr el archivo server.py que es el que se encarga de proporcionar la data al app cliente.

5- Probarla.